module Main where

import Control.Monad (void, forever)
import Control.Applicative ((<$>))
import Data.Random.Source.Std (StdRandom(..))
import Network.MPD (withMPD, idle, Subsystem(PlaylistS))
import RandomAlbums
import System.Environment (getArgs, getProgName)
import System.Exit (exitWith, ExitCode(..))


-- Watch the MPD playlist forever.
-- If the length of the playlist falls below a limit, add a random album
main :: IO ()
main = do
    args <- getMinLength <$> getArgs
    case args of
        Just minLength -> forever . withMPD $ do
            onLowPlaylist minLength $ addRandomAlbum StdRandom
            void $ idle [PlaylistS]
        _ -> usage >> die

getMinLength :: [String] -> Maybe Integer
getMinLength [] = Just 100
getMinLength [num] = case reads num of
    [(minLength, "")] -> Just minLength
    _                 -> Nothing
getMinLength _ = Nothing


usage, version, exit, die :: IO()
usage   = getProgName >>= \name -> putStrLn ("Usage: " ++ name ++ " [length]")
version = getProgName >>= \name -> putStrLn (name ++ " 0.1.0.0")
exit    = exitWith ExitSuccess
die     = exitWith (ExitFailure 1)
