{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}

module RandomAlbums where

import Control.Applicative ((<$>))
import Control.Concurrent (threadDelay)
import Control.Monad (when)
import Control.Monad.Trans (liftIO)

import Data.List (nub, (\\))
import Data.Random (runRVar)
import Data.Random.Extras (safeChoice)

import Network.MPD

getAlbum :: Song -> Value
getAlbum song = case sgGetTag Album song of
    Just (x:_) -> x
    _          -> ""

-- Execute a callback if the playlist is shorter than the given length
onLowPlaylist :: Integer -> MPD () -> MPD ()
onLowPlaylist limit callback = do
    playlistLength <- fmap stPlaylistLength status
    when (playlistLength <= limit) callback

-- Albums with " in their name can not be added via queries
-- https://github.com/vimus/libmpd-haskell/issues/83
isValidAlbum :: Album -> Bool
isValidAlbum = not . ('"' `elem`) . toString

-- Add a random album
-- addRandomAlbum :: (RandomSource ?? s, MonadMPD m) => s -> m ()
addRandomAlbum randomSource = do
    -- Wait a bit, otherwise ncmpcpp can crash :(
    liftIO $ threadDelay 1

    currentAlbums <- nub . map (getAlbum) <$> playlistInfo Nothing
    albums <- (\\ currentAlbums) <$> filter isValidAlbum <$> getAlbums

    case safeChoice albums of
        Nothing -> do
            liftIO $ putStrLn "Nothing to add"
            return ()
        Just randomChoice -> do
            randomAlbum <- liftIO $ runRVar randomChoice randomSource
            liftIO $ putStrLn ("Adding album" ++ (show randomAlbum))
            addAlbum randomAlbum

addAlbum :: Value -> MPD ()
addAlbum = findAdd . (Album =?)

-- Get all the albums in the MPD database
getAlbums :: MPD [Value]
getAlbums = list Album Nothing
